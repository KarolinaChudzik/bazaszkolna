import sys

phrase = sys.argv[1]

ALLOWED_USER_TYPES = ("Wychowawca", "Nauczyciel", "Mentor", "Uczen", "Koniec")
class_no = {}


class Wychowawca:

    def __init__(self):
        self.no_class = {}
        self.students_in_class = []

    def get_supteacher_data(self):
        self.class_names = []
        supteacher = input("Podaj imie i nazwisko: ")
        while True:
            class_name = input("Podaj nazwe prowadzonej klasy: ")
            if not class_name:
                break
            self.class_names.append(class_name)
            if class_name:
                class_no[class_name] = {"Wychowawca": supteacher, "Uczniowie": []}
        self.no_class[supteacher] = {"Prowadzone klasy: ": self.class_names}
        print(f"Numery klas prowadzone przez wychowace {self.class_names} "
              f"zostaly dodane do listy")
        print(class_no)


class Nauczyciel:

    def __init__(self):
        self.teacher_data = {}

    def get_data(self):
        self.no_class = []
        subject = input("Podaj nazwe przedmiotu:")
        tch_no_classes = input("Podaj numer klasy: ")
        self.no_class.append(tch_no_classes)
        while tch_no_classes != '':
            tch_no_classes = input("Podaj numer klasy: ")
            self.no_class.append(tch_no_classes)
        self.teacher_data[subject] = {"Rodzaj uzytkownika: ": user_type,
                                      "Numer klas: ": self.no_class}
        print(f"Nazwa przedmiotu {subject} oraz klasy {self.no_class} "
              f"zostaly dodane do listy.")


class Uczen:

    def __init__(self):
        self.student_data = {}

    def get_student_data(self):
        name = input("Podaj imie i nazwisko: ")
        st_no_classes = input("Podaj numer klasy: ")
        if st_no_classes:
            class_no[st_no_classes]["Uczniowie"].append(name)
        self.student_data[name] = {"Rodzaj uzytkownika:": user_type,
                                 "Numer klasy": st_no_classes}
        print(f"Uczen {name} z klasy {st_no_classes} dodany do listy.")


while True:
    user_type = input("Podaj typ uzytkownika:")
    if user_type not in ALLOWED_USER_TYPES:
        print(f"Nieprawidlowy typ uytkownika, sprobuj ponownie. "
              f"Dostepne nazwy uzytkownikow: {ALLOWED_USER_TYPES}")
        continue
    elif user_type == "Koniec":
        print("Zakonczono prace programu.")
        break
    else:
        if user_type == "Wychowawca":
            stch = Wychowawca()
            stch.get_supteacher_data()
        if user_type == "Nauczyciel" or user_type == "Mentor":
            tch = Nauczyciel()
            tch.get_data()
        if user_type == "Uczen":
            st = Uczen()
            st.get_student_data()


if phrase == "klasy":
    print(class_no)
elif phrase == "wychowawca":
    print(stch.no_class)
elif phrase == "nauczyciel":
    print(tch.teacher_data)
elif phrase == "uczen":
    print(st.student_data)

